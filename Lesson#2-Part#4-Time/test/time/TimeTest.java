package time;

import static org.junit.Assert.*;


import org.junit.Test;

/*
 * author: Sarthak Dhingra
 * 
 * 
 * */

public class TimeTest {

	@Test
	public void testGetTotalSecondsRegular() {
		
		int totalSeconds= Time.getTotalSeconds("01:01:01");
		assertTrue("The time provided does not match the result", totalSeconds == 3661);
	
	}
	
	@Test (expected=NumberFormatException.class)
	public void testGetTotalSecondsException() {
		int totalSeconds=Time.getTotalSeconds("01:01:0A");
		fail("The time provided is not valid");
	}
	
	@Test 
	public void testGetTotalSecondsBoundaryIn() {
		int totalSeconds=Time.getTotalSeconds("00:00:59");
		assertTrue("The time provided does not match the result", totalSeconds == 59);
	}
	
	@Test (expected=NumberFormatException.class)
	public void testGetTotalSecondsBoundaryOut() {
		int totalSeconds=Time.getTotalSeconds("01:01:60");
		fail("The time provided is not valid");
	}
	
	
// TOTAL MILLISECONDS TESTS	
	
	@Test 
	public void testGetTotalMillisecondsRegular() {
		int totalMilliseconds=Time.getTotalMilliseconds("12:05:50:05");
		assertTrue("Invalid number of milliseconds",totalMilliseconds==5);
		
	}
	
	
	@Test (expected=NumberFormatException.class)
	public void testGetTotalMillisecondsException() {
		int totalMilliseconds=Time.getTotalMilliseconds("12:05:50:0A");
		fail("Invalid number of milliseconds");
		
	}
	
	
	// good example as "999" needs substring from 9-12 (12 exclusive) but changing that the regular and exception tests will break
	
	@Test 
	public void testGetTotalMillisecondsBoundaryIn() {
		int totalMilliseconds=Time.getTotalMilliseconds("12:05:50:999");
		assertTrue("Invalid number of milliseconds",totalMilliseconds==999);
		
	}
	
	
	// again good example need to refractor the code to send numberformat exception in case of 1000 milliseconds
	@Test (expected=NumberFormatException.class)
	public void testGetTotalMillisecondsBoundaryOut() {
		int totalMilliseconds=Time.getTotalMilliseconds("12:05:50:1000");
		fail("The time provided is not valid");
		
	}
	
	
	

}
